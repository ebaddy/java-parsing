/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package belajarparsing;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;

/**
 *
 * @author ASUS
 */
public class ParsingMassage {
    
    public static void main(String[] args) throws IOException, ISOException {
        ParsingMassage iSOMessage = new ParsingMassage();
        Map<String, String> bitMap = new HashMap<>();

        String isoX[] = {
            //Inquiry Request Message
            //"21004030004180810000059950200000024879020190103174132602107451001707456789800000000000000480310000000014288007110000000000000"
            
            //Inquiry Response Message
            //"21104030004182810000059950200000024879020190103174132602107451001707456789800000000000000000048144451CA0101428800711534100000023042195442007642936O501366097251520SYM21SB2166E41767643019718973BADRS S'AMIRI, SH            R300000130020000000000"
            
            //Purchase Request Message
            "22005030004180810004059950236000000000500000000002497082019011021022760210745100170745678980000000000000048145451CA01014288007115341000000230830213U84349I24326747373235196080SYM21SB21600052264540686E5A5328DRS S'AMIRI, SH            R30000013002000000000000505351128022-1234567    0012020000002000000000050000"
        };

        for (int i = 0; i < isoX.length; i++) {
            try {
                System.out.println("ISO MESSAGE : ");
                System.out.println(isoX[i]);
                System.out.println("LENGTH : " + isoX[i].length());
                System.out.println();
                
                // parse each bitmap
                bitMap = iSOMessage.getBitmap(isoX[i]);
//                
//                
//                if (bitMap.get("MTI").equalsIgnoreCase("2100") && bitMap.get("2").contains("99502")) {
//                    // Inquiry Request Message
//                    String[] _48 = iSOMessage.inquiryRequestPrepaidUnpackBit48(bitMap.get("48"), "0000", 2100);
//                }else if(bitMap.get("MTI").equalsIgnoreCase("2110") && bitMap.get("2").contains("99502")) {
//                    // Inquiry Response Message
//                    String[] _48 = iSOMessage.inquiryResponsePrepaidUnpackBit48(bitMap.get("48"), "0000", 2110);
//                }else if(bitMap.get("MTI").equalsIgnoreCase("2200") && bitMap.get("2").contains("99502")){
//                    // Inquiry Payment Message
//                    String[] _48 = iSOMessage.inquiryPaymentPrepaidUnpackBit48(bitMap.get("48"), "0000", 2110);
//                }
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public Map<String, String> getBitmap(String data) throws ISOException {
        // Create Packager based on XML that contain DE type
        Map<String, String> bitMap = new HashMap<String, String>();
        GenericPackager packager = new GenericPackager("isoSLSascii.xml");

        // Create ISO Message
        ISOMsg isoMsg = new ISOMsg();
        isoMsg.setPackager(packager);

        isoMsg.unpack(data.getBytes());
        String bitmapString = isoMsg.getValue(-1).toString();
        bitmapString = bitmapString.replace("}", "");
        bitmapString = bitmapString.replace("{", "");
        String[] bitmapArray = bitmapString.split(",");

        System.out.println("<-- START DATA FIELD PARSING -->");
        bitMap.put("MTI", isoMsg.getMTI());
        System.out.println("MTI : " + bitMap.get("MTI"));

        for (int i = 0; i < bitmapArray.length; i++) {
            String bitId = bitmapArray[i].trim();

            bitMap.put(bitId, isoMsg.getString(bitId));
            System.out.println("BIT " + bitId + " : " + bitMap.get(bitId) + ":" + bitMap.get(bitId).length());
        }
        
        System.out.println("<-- END DATA FIELD PARSING -->");
        return bitMap;
    }
    
    public String[] inquiryRequestPrepaidUnpackBit48(String msg, String rc, int getMti) {
        System.out.println("[x] B00000IT 48 : " + msg);
        System.out.println();
        
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
        
        // Inquiry Request Message
        int[] main = {7, 11, 12, 1};
        
        int indexData = 0;
        
        // Inquiry Request Message
        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag"
        };      
        
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            
            System.out.println("<-- START BIT 48 PARSING -->");
            
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                
                n = l;
            }
            
            System.out.println("<-- END BIT 48 PARSING -->");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
        return hasilX;
    }

    public String[] inquiryResponsePrepaidUnpackBit48(String msg, String rc, int getMti) {
        System.out.println("[x] B00000IT 48 : " + msg);
        System.out.println();
        
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
                
        // Inquiry Response Message
        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9, 1, 10};
        
        int indexData = 0;
        
        // Inquiry Response Message
        
        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_reference_number",
            "switcher_reference_number",
            "subscriber_name",
            "subscriber_segmentation",
            "power_consuming_category",
            "minor_unit_of_admin_charger",
            "admin_charges"
        };        
        
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            
            System.out.println("<-- START BIT 48 PARSING -->");
            
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                
                n = l;
            }
            
            System.out.println("<-- END BIT 48 PARSING -->");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
        return hasilX;
    }

    public String[] inquiryPaymentPrepaidUnpackBit48(String msg, String rc, int getMti) {
        System.out.println("[x] B00000IT 48 : " + msg);
        System.out.println();
        
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
        
        // Inquiry Payment Message
        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9, 1, 10, 1};
        
        int indexData = 0;
        

        // Inquiry Payment Request Message
        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_reference_number",
            "switcher_reference_number",
            "subscriber_name",
            "subscriber_segmentation",
            "power_consuming_category",
            "minor_unit_of_admin_charger",
            "admin_charges",
        "buying_option"
        };
        
        
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            
            System.out.println("<-- START BIT 48 PARSING -->");
            
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-15s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                
                n = l;
            }
            
            System.out.println("<-- END BIT 48 PARSING -->");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        
        return hasilX;
    }
}
